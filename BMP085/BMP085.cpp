#include "BMP085.h"

BMP085::BMP085(HardwareI2c &i2c)
    : _i2c(i2c), _samplingMode(STANDARD)
{
  // Set i2c device address
  _i2c.setSlaveAddress(this->_deviceAddress);

  // Calibrate the sensor
  _calibrate();
}

BMP085::~BMP085()
{
  // Left empty
}

void BMP085::_calibrate()
{
  // Assemble the calibration values
  _AC1 = (_i2c.read(AC1_MSB) << 8) + _i2c.read(AC1_LSB);
  _AC2 = (_i2c.read(AC2_MSB) << 8) + _i2c.read(AC2_LSB);
  _AC3 = (_i2c.read(AC3_MSB) << 8) + _i2c.read(AC3_LSB);
  _AC4 = (_i2c.read(AC4_MSB) << 8) + _i2c.read(AC4_LSB);
  _AC5 = (_i2c.read(AC5_MSB) << 8) + _i2c.read(AC5_LSB);
  _AC6 = (_i2c.read(AC6_MSB) << 8) + _i2c.read(AC6_LSB);
  _B1 = (_i2c.read(B1_MSB) << 8) + _i2c.read(B1_LSB);
  _B2 = (_i2c.read(B2_MSB) << 8) + _i2c.read(B2_LSB);
  _MB = (_i2c.read(MB_MSB) << 8) + _i2c.read(MB_LSB);
  _MC = (_i2c.read(MC_MSB) << 8) + _i2c.read(MC_LSB);
  _MD = (_i2c.read(MD_MSB) << 8) + _i2c.read(MD_LSB);
}

double BMP085::temperature()
{
  // Tell the sensor to measure the temperature
  _i2c << 0xF4 << 0x2E;
  _i2c.write();

  // Sleep for the max. conversion time
  usleep(4.5 * 1000);

  // Read the raw temperature value
  int16_q UT = (_i2c.read(0xF6) << 8) + _i2c.read(0xF7);

  // Calculate the real temperature
  int32_q X1 = ((UT - _AC6) * _AC5) >> 15;
  int32_q X2 = (_MC << 11) / (X1 + _MD);
  _B5 = X1 + X2;

  // Return temperature in °C
  return static_cast<double>(((_B5 + 8) >> 4)) / 10.0f;
}

uint32_q BMP085::pressure()
{
  // Tell the sensor to measure the pressure
  _i2c << 0xF4 << (0x34 + (_samplingMode << 6));
  _i2c.write();

  // Sleep for the right conversion time
  switch (this->_samplingMode)
  {
    case ULTRA_LOW_POWER:
      usleep(4.5 * 1000);
      break;
    case STANDARD:
      usleep(7.5 * 1000);
      break;
    case HIGH_RESOLUTION:
      usleep(13.5 * 1000);
      break;
    case ULTRA_HIGH_RESOLUTION:
      usleep(25.5 * 1000);
      break;
  }

  // Read the raw pressure value
  int32_q UP = ((_i2c.read(0xF6) << 16) + (_i2c.read(0xF7) << 8) + _i2c.read(0xF8)) >> (8 - _samplingMode);

  // To calculate the pressure a temperature
  // measurement must be done before
  this->temperature();

  // Calculate the real pressure
  int32_q B6 = _B5 - 4000;
  int32_q X1 = (_B2 * (B6 * (B6 >> 12))) >> 11;
  int32_q X2 = (_AC2 * B6) >> 11;
  int32_q X3 = X1 + X2;
  int32_q B3 = (((_AC1 * 4 + X3) << _samplingMode) + 2) / 4;
  X1 = _AC3 * (B6 >> 13);
  X2 = (_B1 * (B6 * (B6 >> 12))) >> 16;
  X3 = (X1 + X2 + 2) >> 2;
  uint32_q B4 = (_AC4 * (X3 + 32768)) >> 15;
  uint32_q B7 = (UP - B3) * (50000 >> _samplingMode);

  // Init pressure
  int32_q pressure = 0;

  if (B7 < 0x80000000)
    pressure = (B7 * 2) / B4;
  else
    pressure = (B7 / B4) * 2;

  X1 = (pressure >> 8) * (pressure >> 8);
  X1 = (X1 * 3038) >> 16;
  X2 = (-7357 * pressure) >> 16;
  pressure += (X1 + X2 + 3791) >> 4;

  // Return the pressure in hPa
  return pressure / 100;
}

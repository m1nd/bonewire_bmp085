#ifndef BMP085_H
#define BMP085_H

//
// Include
//
#include "Bonewire/I2c/Hardware/HardwareI2c.h"

//
// Using
//
using namespace Bonewire;

// --------------
//  SamplingMode
// --------------
//
enum SamplingMode
{
  ULTRA_LOW_POWER = 0, STANDARD, HIGH_RESOLUTION, ULTRA_HIGH_RESOLUTION
};

//
// Defines
//
#define AC1_MSB 0xAA
#define AC1_LSB 0xAB
#define AC2_MSB 0xAC
#define AC2_LSB 0xAD
#define AC3_MSB 0xAE
#define AC3_LSB 0xAF
#define AC4_MSB 0xB0
#define AC4_LSB 0xB1
#define AC5_MSB 0xB2
#define AC5_LSB 0xB3
#define AC6_MSB 0xB4
#define AC6_LSB 0xB5
#define B1_MSB  0xB6
#define B1_LSB  0xB7
#define B2_MSB  0xB8
#define B2_LSB  0xB9
#define MB_MSB  0xBA
#define MB_LSB  0xBB
#define MC_MSB  0xBC
#define MC_LSB  0xBD
#define MD_MSB  0xBE
#define MD_LSB  0xBF

//
// Example usage:
//
// HardwareI2c i2c(I2C_1);
// BMP085 bmp085(i2c);
// bmp085.setSamplingMode(ULTRA_HIGH_RESOLUTION);
//
// while (true)
// {
//   std::cout << "Temperature: " << bmp085.temperature() << " °C" << std::endl;
//   std::cout << "Pressure: " << bmp085.pressure() << " hPa" << std::endl;
//   sleep(1);
// }
//

class BMP085
{
private:
  /**
   * I2C device address
   */
  const static byte_q _deviceAddress = 0x77;

  /**
   * I2c bus
   */
  HardwareI2c _i2c;

  /**
   * Sampling mode
   */
  SamplingMode _samplingMode;

  /**
   * AC1
   */
  int16_q _AC1 = 0;

  /**
   * AC2
   */
  int16_q _AC2 = 0;

  /**
   * AC3
   */
  int16_q _AC3 = 0;

  /**
   * AC4
   */
  uint16_q _AC4 = 0;

  /**
   * AC5
   */
  uint16_q _AC5 = 0;

  /**
   * AC6
   */
  uint16_q _AC6 = 0;

  /**
   * B1
   */
  int16_q _B1 = 0;

  /**
   * B2
   */
  int16_q _B2 = 0;

  /**
   * B5 (comes from temperature calculation)
   */
  int16_q _B5 = 0;

  /**
   * MB
   */
  int16_q _MB = 0;

  /**
   * MC
   */
  int16_q _MC = 0;

  /**
   * MD
   */
  int16_q _MD = 0;

  /**
   * Copy constructor
   */
  BMP085(const BMP085 &original);

  /**
   * Assignment operator
   */
  BMP085 &operator=(const BMP085 &original);

  /**
   * Calibrate the sensor device.
   *
   * @return void
   */
  void _calibrate();

public:
  /**
   * Default constructor
   */
  BMP085(HardwareI2c &i2c);

  /**
   * Default destructor
   */
  ~BMP085();

  /**
   * Get the current temperature value in °C.
   *
   * @return Temperature
   */
  double temperature();

  /**
   * Get the current pressure value in hPa.
   *
   * @return Pressure
   */
  uint32_q pressure();

  /**
   * Set the sensor device's sampling mode.
   *
   * @param samplingMode Sampling mode
   * @return             void
   */
  void setSamplingMode(SamplingMode samplingMode)
  {
    this->_samplingMode = samplingMode;
  }

  /**
   * Get the sensor device's sampling mode.
   *
   * @return Sampling mode
   */
  SamplingMode getSamplingMode()
  {
    return this->_samplingMode;
  }
};

#endif // BMP085_H
